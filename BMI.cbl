       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. THANAWAT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT    PIC 999V99 VALUE ZEROs.
       01  HIGH      PIC 999 VALUE ZEROS.
       01  BMI       PIC 99V99 VALUE ZEROS.
       01  RESULT    PIC X(25).

       PROCEDURE DIVISION.
       BEGIN.
           PERFORM DAT
           DISPLAY "BMI Calculator"
           PERFORM DAT
           DISPLAY "Enter value of Weight (kg.) - " 
           WITH NO ADVANCING ACCEPT WEIGHT 
           DISPLAY "Enter value of High (cm.) - "
           WITH NO ADVANCING ACCEPT HIGH 
           PERFORM DAT
           
           COMPUTE BMI = (WEIGHT / ((HIGH/100)*(HIGH/100)))
           
           IF BMI LESS THAN 18.5 THEN
              MOVE "Under Weight" TO RESULT
           ELSE
              IF BMI GREATER THAN OR EQUAL TO 18.5 AND BMI LESS THAN OR 
              EQUAL TO  22.9 THEN
                 MOVE "Normal" TO RESULT
              ELSE
                 IF BMI GREATER THAN OR EQUAL TO 23 AND BMI LESS THAN 
                    OR EQUAL TO  24.9 THEN
                       MOVE "Over Weight" TO RESULT
                 ELSE 
                    IF BMI GREATER THAN OR EQUAL TO 25 AND BMI LESS THAN 
                    OR EQUAL TO  29.9 THEN
                       MOVE "Obese" TO RESULT
                    ELSE
                       IF BMI GREATER THAN OR EQUAL 30 THEN
                          MOVE "Extremly Obese" TO RESULT
                       ELSE
                          MOVE "Something Wrong!" TO RESULT
                       END-IF 
                    END-IF
                 END-IF
              END-IF
           END-IF    
           DISPLAY "RESULT"
           PERFORM DAT
           DISPLAY "BMI: " BMI  
           DISPLAY "Result: " RESULT
           PERFORM DAT 
           DISPLAY "PERSONAL DATA"
           PERFORM DAT 
           DISPLAY "Weight: " WEIGHT " kg."
           DISPLAY "High: " HIGH " cm."
      *    PERFORM DAT 
           DISPLAY ""
           GOBACK        

           
           .
       DAT.
           DISPLAY "----------"
           EXIT
           .
       DOK.
           DISPLAY "**********"
           EXIT
           . 
        